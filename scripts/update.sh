#!/usr/bin/env bash
can_overwrite() {
  if [ -e $1 ]
  then
    read -p "Are you sure you want to overwrite ${1}? " -r

    if [[ $REPLY =~ ^[Yy]$ ]]
    then
      return 0
    else
      return 1
    fi
  else
    return 0
  fi  
}

DIR="$(realpath $(dirname "$0"))"
SOURCES_DIR="$(dirname $DIR)/home/"
sources=$($DIR/sources.sh)
for path in $sources
do
  file=$(echo "$path" | sed "s|^$SOURCES_DIR||")
  to="${HOME}/$file"

	echo "path: $path"
	echo "to: $to"
  if can_overwrite $to
  then
    rm -rf "${HOME}/$file"
    ln -s "$path" "$to"
  fi
done

echo

