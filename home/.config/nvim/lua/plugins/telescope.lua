return {
	'nvim-telescope/telescope.nvim', tag = '0.1.8',
	cmd = "Telescope",
	dependencies = { 'nvim-lua/plenary.nvim' },
	-- More key ideas https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/plugins/extras/editor/telescope.lua#L92
	keys = {
		{ "<leader><space>", "<cmd>Telescope find_files<cr>", desc = "Find Files (Root Dir)" },
		{ "<leader>ff", "<cmd>Telescope find_files<cr>", desc = "Find Files (Root Dir)" },
		{ "<leader>fg", "<cmd>Telescope live_grep<cr>", desc = "Grep (Root Dir)" },
		{ "<leader>fb", "<cmd>Telescope buffers<cr>", desc = "Buffers" },
		{ "<leader>fh", "<cmd>Telescope help_tags<cr>", desc = "Help Pages" },
	}
}

