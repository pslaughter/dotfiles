#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

source $DIR/aliases.sh
source $DIR/aliases-custom.sh

export PATH=$PATH:$DIR/bin

function compare-diffs() {
	ref_a=$1
	ref_b=$2
	ref_a_base=$(git merge-base origin/master ${ref_a})
	ref_b_base=$(git merge-base origin/master ${ref_b})
	echo "Comparing ${ref_a_base}...${ref_a} with ${ref_b_base}...${ref_b}"
	echo "vimdiff <(git diff ${ref_a_base}...${ref_a}) <(gif diff ${ref_b_base}...${ref_b})"
}
