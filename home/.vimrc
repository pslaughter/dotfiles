set nocompatible
filetype plugin indent on

" FUNCTIONS ------------------------

function! s:goyo_enter()
  if has('gui_running')
    set guioptions-=m
    set guioptions-=T
  endif
endfunction

function! s:goyo_leave()
  if has('gui_running')
    set guioptions+=m
    set guioptions+=T
  endif
endfunction

" HOOKS ----------------------------

autocmd! User GoyoEnter nested call <SID>goyo_enter()
autocmd! User GoyoLeave nested call <SID>goyo_leave()

augroup pencil
	autocmd!
augroup END

" LOAD PLUGINS ---------------------

so ~/.vim/plugins.vim

" DEFAULTS -------------------------

colorscheme gruvbox
set background=dark
set noexpandtab
set tabstop=2
set shiftwidth=2
set clipboard=unnamed
set backspace=indent,eol,start

" GENERAL CONFIGURATION -------------

au Filetype javascript setlocal shiftwidth=2 tabstop=2 expandtab

let g:javascript_plugin_jsdoc = 1
let g:xml_syntax_folding = 0

" SYNTASTIC CONFIGURATION -----------

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_aggregate_errors = 1
" let g:syntastic_javascript_checkers = ['eslint']

" DIFF CONFIGURATION -------
if has("patch-8.1.0360")
    set diffopt+=internal,algorithm:patience
endif

